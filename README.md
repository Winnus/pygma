# PyGMA

Pythonic Genetic MPI paralelized Algorithm 


# PyGMA Documentation

### Words
Individuals: Individual with its own genome and a fitness value. It represents one solution in the search space.

Population: an ensemble of many individuals

Operator Stack: stack of genetic operators that will be applied sequentially on the genes of a population.

PyGMA (Pythonic Genetic MPI Parallelized Algorithm) is a genetic algorithm.

### Configuration
Everything is configured inside the 'config.py' file.
At first the Genetic operators to use will be instantiated.
This can/should be user defined classe which are located inside the 'genetic_operators.py' file.
For more information on these see [Genetic Operators](#Genetic-Operators)

Evolutionary Phases represent an evolving phase [Evolutionary Phases](#Evolutionary-Phases).
Many of these phases can be stacked together.
This enables to first evolve simpler solutions then continue to evolve complex ones.  

### Genetic Operators
Genetic operators are used to change the genome of individuals or to produce new individuals by combining the genome of multiple individuals.
Genetic operators are defined inside the 'genetic_operators.py' file.
Here an informal interface is used.
The operators should at least implement the function ```def operate(self, old_population, new_population) -> list:```.
This function will be called to apply the genetic operator.
In PyGMA the genetic operators for a population (PyGMA can handle multiple island models) can be stacked to an "Operator Stack".
This stack is then be applied sequentially on the genes of the population.
Hence there can be two states of the genes:
1. Before an operator from the stack has been applied. Here the fitness of the genes is evaluated.
2. An operator from the stack has made changes to the genes and we do not know the fitness anymore.

Therefore the function has two arguments:
 1. old_population: list of numpy.array(dtype=numpy.bool_) genes genes before the operator stack is applied, sorted by their fitness.
 Which means that one can be sure that if one picks the first two genes there are the currently best performing ones.
 
 2. new_population: list  current state of modifications in operator stack

The output of the operator should be a list of genes that should be used in the future.
ONLY THESE GENES WILL BE USED IN THE FUTURE.
Therefore an operator should always append its new genes to the ```new_population``` list.
He can remove items from the list but then the size of the population will be shrinked.
Examples can be viewed in the genetic operator definition file.
They will be imported, defined and used in the config file by the user.


### Evolutionary Phases
Evolutionary phases make it simpler to evolve a difficult solution that can be gradually evolved.
First one might evolve something that is relative simple and then put it into a different environment to evolve it further.
This can be achieved by combining multiple evolutionary phases.
The phases are realized through an informal interface.
Each phase should inherit from the base class ```Evolutionary_phase```.
It should then implement the  ```def completed(self, population_fitness: list, epochs: int) -> bool:``` function.
This function will receive a list of the population fitness ```[f_p0, f_p1, ..., f_pn]``` where f_pn is the fitness of the best performing individual in the island population n.
```epochs``` is the current epoch of the phase.
A boolean value should be returned indicating if the phase is completed (1) or not (0).

Phases have certain properties.
They contain a name, phase start operators and an experiment.
The phase start operators will be applied to all island populations when the phase begins.
This is usefull e.g. if one wants to extend the gene string with an gene string extension operator.

An experiment will hold all the necessary implementation to instanciate and test individuals.
Hence they allowing the phase to controll which evaluation of the genes will be conducted during an epoch.


### Experiments
These are defined in the ```experiment.py``` file.
Experiments should ideally inherit from the ```Experiment``` class.
As such they need to implement the function ```def conduct(self, gene) -> float:```.
This function gets a gene, which is a boolean array of the type```numpy.array(dtype=numpy.bool_)```.
From this it should create/instantiate (genotype -> phenotype) an individual on which the experiment is conducted.
By conductiong the experiment the a fitness value is generated for the gene.
This should be returned.

# Usage Guide
This section explains how to step by step use PyGMA.
PyGMA works with user defined components/classes(
![Graphical explanation of the user interface](User_interface.pdf)).
As such one needs to define the components one want to use.
Then set them up insid the configuration file and tell PyGMA to use this configuration.

### 1. Define Evolutionary Phase
The first step is to define the evolutionary phase.
This component will contain the experiment component for the individuals and the stop conditions for the evolutionary process.
It will be defined in the file ```components/evolutionary_phase.py```

As example we create the following phase:

```
class Simple_evolutionary_phase(Evolutionary_phase):

    def completed(self, population_fitness: list, epochs: int) -> bool:
        """Called to check if the phase has reached its end."""
        # population_fitness will be a list with the max fitness value in 
        # each island population
        max_fitness = max(population_fitness)
        if max_fitness > 90.9:
            return True

        if epochs > 300:
            return True

        return False
```
This class inherits all needed variables from the ```Evolutionary_phase``` base class.
We only define the stop condition by overriding the ```completed(self, population_fitness: list, epochs: int) -> bool``` function.
This function will be called by PyGMA to check if the evolutionary process has finised.
As such we define that if the maximal fitness for all populations is > 90.9 or the evolutionary epochs are > 300 we want to stop the process.


### 2. Define Genetic Operators
Next we going to define Genetic Operators (GO).
These will be applied to modifie the gene strings of the individuals.
GO can be stacked into list (operator stack) and each operator in that lis is applied in sequence one after another.
All operators will mainly define then function ```operate(self, old_population, new_population)```.
Here ```old_population``` are the genes that are untouched by the GO and are sorted in ascending order based on their fitness, meaning ```old_population[0]``` is the gene with the highest fitness.
```new_population``` is a set of genes which where altered or produced by the genetic operator stack i.e. current state of modifications in operator stack.
GO's are defined in the file ```components/genetic_operators.py```

For example we could define a binary (meaning that it will work with binary genes like gene="100101010001001") mutation operator:

```
class Mutation_operator(Genetic_operator):
    """
    This class represents a binary mutation operator.
    It will produce n new individuals by mutating n genes with
    the propability defined by mutation rate
    """

    def __init__(self, mutation_rate, new_individuals, rng_seed=9):
        self.mutation_rate = mutation_rate
        # how many new individuals should this operator produce?
        self.new_individuals = new_individuals
        self.rng = np.random.default_rng(rng_seed)

    def operate(self, old_population, new_population):
        """
        Apply mutation to all genes of the current op stack.
        """
        # take n individuals from the old_population (sorted by fitness) and mutate them
        for n in range(self.new_individuals):
            gene = np.copy(old_population[n])
            gene_size = len(gene)
            # mutation indexes
            # having at least one mutation
            mutations = int(self.mutation_rate*gene_size)
            if mutations == 0:
                mutations = 1
            indexes = self.rng.integers(low=0, high=gene_size, size=mutations)
            # flip the bits at indexes
            # note, ~ is like np.invert()
            gene[indexes] = ~gene[indexes]
            # append the new individual
            new_population.append(gene)
        return new_population
```


### 3. Define Experiment
If we have sucessfully defined all the operators we want to use then we can define our experiment.
An experiment is a test in which the gene is evaluated and rated with a fitness.
It can be an optimisation task, it can be that one wants to evolve cratures that can swim [https://www.karlsims.com/evolved-virtual-creatures.html](https://) or any other imagination.
Experiments will be defined in the file ```components/experiment.py```

For simplicity reasons we will define a simple dummy function optimisation experiment:
We want to find x, y, z such that f(x, y, z) = x*2 + y*3 + z = 424242.

```
class Function_optimisation_experiment(Experiment):
    """
    Find values for the function such that it matches a certain output
    """

    def f(self, x, y, z):
        """The function we want to optimize"""
        return x*2 + y*3 + z
        
    def bool2int(self, x):
        r = 0
        for i, b in enumerate(x):
            if b:
                r += b << i
        return r

    def conduct(self, gene) -> float:
        # split gene into blocks that are binary representation
        # of the numbers
        numbers = np.split(gene, 3)
        # convert the bins to ints
        x = bool2int(numbers[0])
        y = bool2int(numbers[1])
        z = bool2int(numbers[2])

        # define the objective output for our function
        output = 424242

        # calculate the output
        r = self.f(x, y, z)

        # calc error and fitness
        error = abs(r-output)

        # if error is small fitness is high
        # make sure that if error can not be 0 :)
        fitness = 1.0/(error + 0.00000000000000001)

        # return the fitness of this gene to pygma
        return fitness
```
We inherit from the ```Experiment``` class and define the function  ``` def conduct(self, gene) -> float:```
This function will be called by PyGMA on each gene to conduct the experiment and retrieve a fitness value for this gene.



### 4. Define Config
Now that all parts are defined we can definen a config that will make use of all the components we just defined.

we make a new file ```components/config_function_optimisation.py```

Inside this file we put the following content:

```
# import the components we defined for further use
from components.genetic_operators import Mutation_operator, Removal_operator
from components.evolutionary_phase import Simple_evolutionary_phase
from components.experiment import Function_optimisation_experiment


# -----------------------------
# Genetic operator definition
# -----------------------------
# Genetic operators can be used for:
# Populations
# Genetic Phases

# our mutation operator will crate 3 genes with a mutation propability of 6%
# for each bit
OP_MUTATION = Mutation_operator(0.06, 3)

# additionally we will use a crossover operator
OP_CROSSOVER = Single_point_crossover_operator(3)

# since the mutation and crossover operator will create 3+3=6 genes we need to remove 6 genes
# from the population otherwise we will increase the population size in each
# epoch/generation
OP_REMOVAL = Removal_operator(6)



# -----------------------------
# Phase definition
# -----------------------------
# Genetic phases allowing for evolution of individuals in
# changing environments or conditions.

# phase 0
# phase starting operators (applied at phase start)
# we will mutate in the beginning
PHASE0_SOP = [OP_MUTATION]

# phase Experiment (conducted on individuals)
# we will use our Function_optimisation_experiment
PHASE0_EXPERIMENT = Function_optimisation_experiment()

# phase definition
PHASE0 = Simple_evolutionary_phase(
    'Phase_0', PHASE0_SOP, PHASE0_EXPERIMENT)



# -----------------------------
# GA Configuration parameters
# -----------------------------
# These are all general parameters for the Algorithm
# in form of a dictionary
CONFIG_FUNC_OP = {
    # -------------
    # Processes
    # --------------
    # use mpi parallelization
    # If use_mpi4py_futures=False start with:
    # mpiexec -n 3 python PyGMA.py
    # or use threads if not specified via slurm
    # mpiexec -n 3 --use-hwthread-cpus python PyGMA.py
    'use_mpi': False,
    
    # use mpi4py.futures
    # this will dynamically spawn workers.
    # NOTE: you have to execute the programm in this manner:
    # mpiexec -n 3 python -m mpi4py.futures PyGMA.py
    'use_mpi4py_futures': False,

    # if mpi=False, how many local processes to use
    # Note if > 1 it will spawn additonal processes that independently
    # solves the experiment. Spawning takes time (memcopys etc)
    # if the experiment is very simple having only one process
    # handling everyting might be faster.
    # start programm with:
    # python PyGMA.py
    'num_local_processes': 1,


    # -------------
    # Populations
    # --------------
    # how many island populations to use
    # is defined by the genetic operators stack lenght
    # used to recombine/produce, mutate, extend...
    'genetic_operator_stack': [
        # pop 0
        [OP_REMOVAL, OP_CROSSOVER, OP_CROSSOVER],
        # pop 1
        [OP_REMOVAL, OP_CROSSOVER, OP_MUTATION],
        # pop 2
        [OP_REMOVAL, OP_CROSSOVER, OP_MUTATION]
    ],

    # how many individuals per population
    'num_individuals': 30,

    # genome lenght for each individual
    'genome_length': 60,

    # init populations from defined gene strings
    # this will ovveride:
    # num_populations, num_individuals, genome_length
    'use_predefined_defined_genes': False,
    'predefined_genes': [
        # pop 0
        [[0, 0, 1], [0, 1, 1]],
        # pop 1
        [[0, 1, 1], [1, 1, 1]]
    ],

    # -------------
    # Evolutinary Phases
    # --------------
    # defining the phases that will be sequantially evolved
    'evolutionary_phases': [
        PHASE0, PHASE0
    ]

}
```

### 5. Run PyGMA
Now we can run PyGMA to start the evolution.
Since we told it in the configuration file to not use MPI we can start it in a single Python process by running

```python PyGMA.py```



# Todos
- provide a user Documentation that covers and explain all needed steps to define own: Phases, Operators, Experiments
- implement a call to the experiment that handles how to save the fittest gene when evolution is finished.
- Code and repository cleanups
    - this has to be user defined and will save the gene into a users usable format for further use
- implement island fusion at user definable time frames in the evolution
    - provide a standart fusion module that can always be used
    - provide an interface that can be used, like the genetic operators, to allow for user definable fusion rule stacks
- allow for genes to be a user defined type, not hardcoded np.bool_ (int or string makes it much easier for some experiments, though they are binary coded in a computer anyway...)


