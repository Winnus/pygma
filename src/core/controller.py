from core.population import Population
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import time as time
# FIXME:
# implement correct print functions in the classes
# such that print(object) can be called
# implement usefull controller output


class Controller:
    """
    GA Controller.
    Handels the algorithm logic.
    """

    def __init__(self, config, rng_seed=9):
        # config
        self.config = config

        # Population objects
        self.populations = []

        # random number generator
        self.rng = np.random.default_rng(rng_seed)

        # phases
        # self.evolutional_phases = config['evolutionary_phases']

        # init populations
        self.__init_populations()

        # epochs
        self.epochs = 0

    def __init_populations(self):
        """
        Initialize the populations
        """
        if self.config["use_predefined_defined_genes"]:
            # make pops
            for genetic_op_stack, genes in zip(self.config["genetic_operator_stack"], self.config["predefined_genes"]):
                pop = Population(genetic_op_stack, self.rng)
                pop.init_individuals(genomes=genes)
                # append
                self.populations.append(pop)

        else:
            for genetic_operator_stack in self.config["genetic_operator_stack"]:
                # make population
                pop = Population(genetic_operator_stack, self.rng)
                pop.init_individuals(
                    num_individuals=self.config["num_individuals"],
                    genome_length=self.config["genome_length"],
                )
                # append
                self.populations.append(pop)

    def __instantiate_individuals(self, phase_index):
        """
        Will instantiate the individuals in the environment function specified
        in the experiment of the phase.
        Parameters
        ----------
        phase_index: int
            the index of the pase list specified in the confid by the user.
            Used to conduct the correct experiment for the phase.
        """
        # pass genes of all populations to the workers
        # one idea is to pass the indexes, meaning:
        # pass
        # (pop_index, indiv_index, phase_index, gene)
        # receive back
        # (pop_index, indiv_index, fitness)
        # then rewrite the fitness into the population individuals

        # nicer would be to pass the individual itself and having the reference updated
        # this will not work with mpi though

        # this is not the nicest pythonic way with the indexes of the populations
        # but it will work for now
        # FIXME: find a more pythonic way

        # make list that workers should evaluate
        worker_tuples = []
        for i_pop, pop in enumerate(self.populations):
            for i_indiv, indiv in enumerate(pop.individuals):
                worker_tuples.append(
                    (i_pop, i_indiv, phase_index, indiv.genome))

        # instantiation via mpi or local worker
        # ---------------------------------------
        if self.config['use_mpi']:
            # use the mpi4py futures
            # ----------------------
            if self.config['use_mpi4py_futures']:
                # import the MPIPoolExecutor
                # importing more then 1x will use chached module
                from mpi4py.futures import MPIPoolExecutor
                # save worker futures
                futures = []
                # starting a worker for each individual
                # num workers will be handled via mpi
                with MPIPoolExecutor() as executor:
                    for w_tuple in worker_tuples:
                        futures.append(
                            executor.submit(
                                self._instantiate_individual_local_process,
                                pop_index=w_tuple[0],
                                indiv_index=w_tuple[1],
                                phase_index=w_tuple[2],
                                gene=w_tuple[3],
                            )
                        )

                # write back the fitness values into the individuals
                for future in futures:
                    # result will be a tuple (pop_index, indiv_index, fitness)
                    result = future.result()
                    self.populations[result[0]
                                     ].individuals[result[1]].fitness = result[2]

            # use mpi calls
            # -------------
            else:
                # will use the cached module and as such the global intercomm
                from mpi4py import MPI
                from core.mpi_tags import mpi_tags
                comm = MPI.COMM_WORLD
                world_size = comm.Get_size()

                # while we have data distribute it to free workers
                # collect finished data as well
                # a gene is computed once the result is written back
                genes_to_compute = len(worker_tuples)
                while genes_to_compute > 0:
                    # check all workers
                    for worker_index in range(1, world_size):

                        # check if worker can compute new data (Is idle) and we have data to compute
                        if comm.iprobe(source=worker_index, tag=mpi_tags.IDLE) and worker_tuples:
                            # eat up that message to clean the pipeline
                            comm.recv(source=worker_index, tag=mpi_tags.IDLE)
                            # pop the next data to distribute
                            data = worker_tuples.pop()
                            # signalling the worker that it should continue to work
                            comm.send(True, dest=worker_index,
                                      tag=mpi_tags.CONTINUE_PROCESSING)
                            # send data to worker
                            comm.send(data, dest=worker_index,
                                      tag=mpi_tags.DATA)

                        # check if we can retrieve a result from the worker
                        if comm.iprobe(source=worker_index, tag=mpi_tags.DATA_RETURN):
                            # retrieve the result
                            w_result = comm.recv(
                                source=worker_index, tag=mpi_tags.DATA_RETURN)
                            # write the fitness result into the appropriate individual
                            # print(
                            #    f'-------------------root got {w_result} from worker {worker_index}')
                            self.populations[w_result[0]
                                             ].individuals[w_result[1]].fitness = w_result[2]
                            # note that we have computed a gene
                            genes_to_compute -= 1

        # use local processes
        # -------------------
        else:
            # check if we just use the main process for evaluation
            if self.config['num_local_processes'] < 2:
                # evaluate the worker tuples
                for w_tuple in worker_tuples:
                    result = self._instantiate_individual_local_process(
                        pop_index=w_tuple[0],
                        indiv_index=w_tuple[1],
                        phase_index=w_tuple[2],
                        gene=w_tuple[3]
                    )
                    self.populations[result[0]
                                     ].individuals[result[1]].fitness = result[2]

            # we wanna spawn local processes yuppi <3
            else:
                # save worker futures
                futures = []
                # starting a worker for each individual
                with ProcessPoolExecutor(max_workers=self.config['num_local_processes']) as executor:
                    for w_tuple in worker_tuples:
                        futures.append(
                            executor.submit(
                                self._instantiate_individual_local_process,
                                pop_index=w_tuple[0],
                                indiv_index=w_tuple[1],
                                phase_index=w_tuple[2],
                                gene=w_tuple[3]
                            )
                        )

                # write back the fitness values into the individuals
                for future in futures:
                    # result will be a tuple (pop_index, indiv_index, fitness)
                    result = future.result()
                    self.populations[result[0]
                                     ].individuals[result[1]].fitness = result[2]

        # sort the Populations based on their fitness values
        for pop in self.populations:
            pop.sort_individuals()

    def _instantiate_individual_local_process(self, pop_index, indiv_index, phase_index, gene):
        # FIXME:
        # is it really the case that when a new worker is instantiated
        # that it will get a full deep copy of all the objects?
        # As such the experiment for an individual is called
        # on a single, only by this worker used instance of the
        # experiment right?
        # I think so but cannot find the source (despite my brain) that
        # verifyes that.
        """
        Will make an experiment with the gene
        Parameters
        ----------
        pop_index: int
            index of the population inside the controller population list
        indiv_index:  int
            index of the individual inside its population
        phase_index: int
            index of the configuration phase array.
            If you have the config you can get the experiment :)
        gene: list
            gene of the individual (numpy.array(dtype=numpy.bool_))
        """
        # instantiate the experiment and evaluate the individual
        experiment = self.config['evolutionary_phases'][phase_index].experiment

        # conduct the experiment
        # print(pop_index)
        # print('worker controller has',self.epochs,'epochs and is at',self)
        # print(self)
        fitness = experiment.conduct(gene)

        # return result tuple
        return (pop_index, indiv_index, fitness)

    # not used
    def epoch(self):
        """
        One epoch:
        Instantiate individuals and evaluate their fitness.
        Apply operator stack.
        May combine island populations
        """
        # instantiate individuals (get them a fitness value)
        self.__instantiate_individuals()

        # genetic operator stack
        # NOTE: after applying the stack the fitness values
        # will be zero again for all individuals
        self.__apply_operator_stack()

        # fusion islands
        # IMP

    def __apply_operator_stack(self):
        """
        Applying the operator stack for each population.
        """
        for pop in self.populations:
            pop.apply_operator_stack()

    def get_population_fitness(self, mean=False) -> list:
        """
        Returns the fitness of all populations.
        Parameters
        ----------
        mean: bool
            if true return the mean fitness of the populations,
            otherwise the max fitness value
        """
        f = []
        for pop in self.populations:
            f.append(pop.get_fitness(mean=mean))
        return f

    def get_fittest_individuals(n=1) -> list:
        """
        Returns the n fittest individuals from all populations.

        Parameters
        ----------
        n: int
            how many individuals should be returned?
        """
        # get the n fittest individuals from the populations
        # pop.gepfittest
        # recombine them for she fittest
        # add a switch that one can know from which populations they are
        # then an additional array is returned with the pop indexes.

    def evolve(self):
        """
        Evolves the genomes through all phases
        """
        for p_index, evolutionary_phase in enumerate(self.config['evolutionary_phases']):
            # ----------
            # init phase
            # ----------
            print(f'Evolutionary phase: {evolutionary_phase}')
            # reset epochs
            self.epochs = 0

            # call the phase init
            # which will return a new population array
            self.populations = evolutionary_phase.initialize(
                self.populations)

            # apply phase start operators
            for op in evolutionary_phase.phase_start_operators:
                for population in self.populations:
                    population.apply_operator(op)

            # ----------
            # phase epochs
            # ----------
            # save timings
            ts_op_stack = []
            ts_evaluate = []
            while not evolutionary_phase.completed(self.get_population_fitness(), self.epochs):
                # conduct a new epoch
                # -------------------

                # apply genetic operator stack on each population
                # NOTE: after applying the stack the fitness values
                # will be zero again for all individuals
                t_op_stack = time.time()
                self.__apply_operator_stack()
                t_op_stack = time.time() - t_op_stack
                ts_op_stack.append(t_op_stack)

                # instantiate individuals (get them a fitness value)
                # tell them the phase index becaus the worker processes
                # (mpi and local) will have their instance of the
                # phase list and with it know the experiments.
                t_evaluate = time.time()
                self.__instantiate_individuals(phase_index=p_index)
                t_evaluate = time.time() - t_evaluate
                ts_evaluate.append(t_evaluate)

                # check if algorithm stagnates
                # To be Implemented IMP

                # fusion islands 
                # meaning that you take two or more islands and combine them
                # into a new island
                # but what is the condition?
                # whenever a new phase happens the phase can do
                # this on their own.

                # epoch done
                # ----------
                # information
                print(f'epoch {self.epochs}')
                print('--------------')
                print(f't_op({t_op_stack})')
                print(f't_eval({t_evaluate})')
                print(f'Pop max fitness {self.get_population_fitness()}')
                print()

                # increment counter
                self.epochs += 1

            # ----------
            # finalize
            # ----------
            print(f'Finished phase {evolutionary_phase}')
            print('--------------------------------')
            print(f'phase epochs: {self.epochs}')
            print(f'min Operator Stack time: {min(ts_op_stack)}')
            print(f'max Operator Stack time: {max(ts_op_stack)}')
            print(f'mean Operator Stack time: {np.mean(ts_op_stack)}')
            print(f'min Fitness Evaluation time: {min(ts_evaluate)}')
            print(f'max Fitness Evaluation time: {max(ts_evaluate)}')
            print(f'mean Fitness Evaluation time: {np.mean(ts_evaluate)}')
            print(f'Pop max fitness {self.get_population_fitness()}')
            print()
            # print('--------------------------------')

        # Evoluting finised
        # ----------------
        # if mpi intercomm was used we want to shut down the workers
        if self.config['use_mpi'] and not self.config['use_mpi4py_futures']:
            # send stop to all wokers
            from mpi4py import MPI
            from core.mpi_tags import mpi_tags
            comm = MPI.COMM_WORLD
            world_size = comm.Get_size()
            print()
            print('MPI was used, stopping mpi workers...')
            for w_index in range(1, world_size):
                comm.send(False, dest=w_index,
                          tag=mpi_tags.CONTINUE_PROCESSING)
