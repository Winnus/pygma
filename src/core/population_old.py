from individual import Individual


class Population:
    def __init__(self, num_individuals, gene_length, rng, genomes=False):
        self.num_individuals = num_individuals
        self.gene_length = gene_length
        self.rng = rng
        #self.genomes= genomes

        # individual objects
        self.individuals = []
        
        # init individuals randomly
        # or with the provided genomes
        if not genomes:
            self.__init_individuals_random()
        else:
            self.__init_individuals_specific(genomes)

    def __init_individuals_random(self):
        # IMP
        # how to initialize the individual genome length?
        for i in range(self.num_individuals):
            self.individuals.append(Individual())

    def __init_individuals_specific(self, genomes):
        """
        initiates the individuales with the genomes given.
        genomes: 2D array with the genomes
        """
        for gene in genomes:
            self.individuals.append(Individual(gene))
