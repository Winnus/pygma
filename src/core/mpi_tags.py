from enum import IntEnum


class mpi_tags(IntEnum):
    """
    Enums used to specify tags in the mpi process communication.
    """
    CONTINUE_PROCESSING = 10
    DATA = 3
    DATA_RETURN = 4
    IDLE = 9
    EXIT = 6
