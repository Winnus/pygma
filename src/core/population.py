from core.individual import Individual
from components.genetic_operators import Genetic_operator
import numpy as np


class Population:
    def __init__(self, operator_stack, rng):
        # self.num_individuals = 0
        # self.gene_length = 0
        self.rng = rng
        # self.genomes= genomes

        # individual objects
        self.individuals: Individual = []

        # genetic operators
        self.genetic_operator_stack = operator_stack

    def init_individuals(self, genomes=None, num_individuals=None, genome_length=None):
        """
        Initiates the individuales with the genomes given or creates n
        random individuals with a specified genome length.

        Parameters
        ----------
        genomes: numpy.array(dtype=numpy.bool_)
            2D array with the genomes
        num_individuals: Int
            how many random individuals
        genome_length: Int
             length of the genes
        """
        if genomes is None and (num_individuals is None or genome_length is None):
            raise ValueError(
                'Provide a fixed genome or parameters to define them randomly (number and size)')
        if genomes is not None:
            for gene in genomes:
                self.individuals.append(Individual(gene))
        else:
            for i in range(num_individuals):
                genome = self.rng.integers(
                    low=0, high=2, size=genome_length, dtype=np.bool_)
                self.individuals.append(Individual(genome))

    def __get_gene_array(self):
        """
        Will return a list containing genome copies of all the individuals.
        """
        genes = []
        for indiv in self.individuals:
            # copy as otherwise will be modified (C-pointer)
            # make sure that we really have a numpy bool array!
            genes.append(np.array(indiv.genome.copy(), dtype=np.bool_))
        return genes

    def apply_operator_stack(self):
        """
        Applys the operator stack on this populations genes.
        """

        # operators getting the genes only
        # they get a state before the stack and the current stack state
        # hence we save all the genes in two lists and evolve the stack list
        # NOTE: this are all numpy arrays, as such you can modified inplace.
        # however operators might make new arrays and thus make new individuals.
        unmodified_genes = self.__get_gene_array()
        current_stack_state = self.__get_gene_array()

        # apply stack
        for genetic_operator in self.genetic_operator_stack:
            current_stack_state = genetic_operator.operate(
                unmodified_genes, current_stack_state)

        # a new generation is born
        self.individuals = []
        for gene in current_stack_state:
            self.individuals.append(Individual(gene))

    def apply_operator(self, operator: Genetic_operator):
        """
        Applies the genetic operator on all genes of the population
        """
        genes = self.__get_gene_array()
        op_genes = operator.operate(genes, genes)

        # a new generation is born
        # which might have more members then the last one
        # hence create complepely new one
        self.individuals = []
        for gene in op_genes:
            self.individuals.append(Individual(gene))

    def get_fitness(self, mean=False):
        """
        Fitness of the population.
        Parameters:
        -----------
        mean: bool
            if True return the mean fitness
            otherwise the max
        """
        # fitness list
        f = []
        for indiv in self.individuals:
            f.append(indiv.fitness)

        if mean:
            return sum(f)/len(self.individuals)
        return max(f)

    def sort_individuals(self):
        """
        Sorts the individuals in the population based on their
        fitness values.
        """
        self.individuals.sort(
            key=lambda Individual: Individual.fitness, reverse=True
        )
