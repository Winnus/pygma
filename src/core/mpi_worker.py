import time
from mpi4py import MPI
from core.mpi_tags import mpi_tags


class MPI_worker():
    def __init__(self, config, sleep_time=0):
        self.config = config
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.sleep_time = sleep_time
        print(f'initialized MPI worker with rank {self.rank}')

    def start(self):
        # worker
        # loop until stop is signalled
        while True:
            # print(f'worker {self.rank} loop')
            # signaling that we are waiting for new work
            self.comm.isend(None, dest=0, tag=mpi_tags.IDLE)

            # we receive a bool if we shall to continue to process
            # to not eat all CPU ressources we sleep a little if no transmission
            # but this will slow down tremendously,
            # don't sleep when you are supposed to be present
            if self.sleep_time > 0:
                while not self.comm.iprobe(source=0, tag=mpi_tags.CONTINUE_PROCESSING):
                    # sleep
                    # print(f'worker {self.rank} is sleeping')
                    time.sleep(self.sleep_time)
            continue_processing = self.comm.recv(
                source=0, tag=mpi_tags.CONTINUE_PROCESSING)

            # if we shall not continue stop the worker
            if not continue_processing:
                break

            # get the working data
            # format will be (pop_index, indiv_index, phase_index, gene)
            data = self.comm.recv(source=0, tag=mpi_tags.DATA)

            # extract data
            pop_index = data[0]
            indiv_index = data[1]
            phase_index = data[2]
            gene = data[3]

            # instantiate the experiment and evaluate the individual
            experiment = self.config['evolutionary_phases'][phase_index].experiment

            # conduct the experiment
            fitness = experiment.conduct(gene)

            # print(f'worker {self.rank} got {data}')
            # return the result tuple
            self.comm.send((pop_index, indiv_index, fitness),
                           dest=0, tag=mpi_tags.DATA_RETURN)
