import numpy as np


class Individual():
    def __init__(self, genome):
        self.genome = np.array(genome, dtype=np.bool_)
        self.fitness = 0