import numpy as np
from config import CONFIG, check_config
from core.controller import Controller

def mpi_worker(config):
    # instantiate the worker
    from core.mpi_worker import MPI_worker
    worker = MPI_worker(config=config, sleep_time=0)
    worker.start()

    return 0


def controller():
    """
    Main controller logic for the algorithm
    It will be called if the calling process shall be the controller.
    """
    # Init controller
    GAController = Controller(CONFIG, rng_seed=9)

    # print information
    print("Init Evolution controller:")
    print(f'populations: {len(GAController.populations)}')
    for i, pop in zip(range(len(GAController.populations)), GAController.populations):
        print(f'Genetic Operators p{i}: {len(pop.genetic_operator_stack)}')

    # evolution for all phases
    GAController.evolve()
    
    # ----------------------
    # Evolution has finished
    # ----------------------
    print()
    print("Evolve finished (fitness or epoch criterion met)")
    print('-------------------------------------------------')

 
    # todo find the population with the best gene
    # get max fitness values
    # this is started inside the controller in get_fittest_individuals()
    # use the below code there to implement the function...
    fit_per_pop = GAController.get_population_fitness(mean=False)
    # m = max(fit_per_pop)
    # i = np.where(fit_per_pop == m)

    print(f'Population fitness:{fit_per_pop}')
    print()
    print()


#     print(GAController.populations[0].individuals[0].fitness)
#     print(GAController.populations[0].individuals[1].fitness)

#     print(GAController.populations[0].individuals[0].genome)
#     gene = GAController.populations[0].individuals[0].genome

    return


def main():
    # read config
    check_config()
    config = CONFIG

    # check if MPI should be used
    if config["use_mpi"]:
        # if we use the mpi4py futures they will handle their
        # worker spawning themselfes. We just need to be controller :)
        if config["use_mpi4py_futures"]:
            controller()

        # if mpi intercomm calls are used we need to decide if we are
        # worker or controller.
        else:
            # checking the ranks
            from mpi4py import MPI
            comm = MPI.COMM_WORLD
            rank = comm.Get_rank()

            # if mpi worker switch to worker mode
            if rank > 0:
                mpi_worker(config)

            # rank 0 is the controller
            else:
                controller()
    # for no mpi the main process is the controller
    else:
        controller()
    return


if __name__ == "__main__":
    print('Starting PyGMA...')
    main()
