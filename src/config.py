# -----------------------------
# import/set the needed configs
# -----------------------------
from components.config_func_op import CONFIG_FUNC_OP
from components.config_logical_gate_construction import CONFIG_CIRCUIT_EVOLVE
from components.config_logical_gate_construction_no_explicit_input_coding import CONFIG_CIRCUIT_EVOLVE_NO_EXPLICIT_INPUT_CODING


CONFIG = CONFIG_CIRCUIT_EVOLVE
#CONFIG = CONFIG_CIRCUIT_EVOLVE_NO_EXPLICIT_INPUT_CODING










# -----------------------------
# check configured parameters
# -----------------------------
# FIXME: is this needed ant helpfully in the end?

def check_config():
    # IMP
    # will check if all parameters are available and within
    # reasonable range
    # really do this?
    # only for most critical parts that have to fit.

    # check if genetic operators are enough in every phase
    if CONFIG['use_predefined_defined_genes'] and len(CONFIG['genetic_operator_stack']) != len(CONFIG['predefined_genes']):
        print(
            f"Warning: having an genetic operator stack for only {len(CONFIG['genetic_operator_stack'])} but there are genes defined for {len(CONFIG['predefined_genes'])}. Will not use all genes, only those having an operator stack.")

    return

#     return
