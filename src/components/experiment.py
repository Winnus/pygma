import numpy as np

# --------------
# Interface idea
# --------------
class Experiment:
    """
    Class defining an experiment.
    """

    def __init__(self):
        self.inputs = []
        self.expected_outputs = []
        self.environment = None

    def conduct(self, gene):
        """
        Conducts the experiment with the given gene
        Returns the fitness value.
        Parameters
        ----------
        gene: numpy.array(dtype=numpy.bool_)
        For easy objectives just implement your function.
        For complex objective functions involving simulations the
        gene is instantiatet in an environment,
        returning the phenotype from the genotype.
        The phenotype can then be used to process the inputs and
        give back its outputs.
        """
        # Idea
        # -----
        # instantiate individual
        # individual = self.environment.instantiate_individual(gene)

        # test the individual
        # outputs = individual(self.inputs)
        # if outputs == self.expected_outputs:
        #    return 0.999

        # return 0.0

        raise NotImplementedError

# --------------
# Experiments
# --------------

class Function_optimisation_experiment(Experiment):
    """
    Find values for the function such that it matches a certain output
    """

    def f(self, x, y, z):
        return x*2 + y*3 + z
        
    def bool2int(self, x):
        r = 0
        for i, b in enumerate(x):
            if b:
                r += b << i
        return r

    def conduct(self, gene) -> float:
        # split gene into blocks that are binary representation
        # of the numbers
        numbers = np.split(gene, 3)
        # convert the bins to ints
        x = bool2int(numbers[0])
        y = bool2int(numbers[1])
        z = bool2int(numbers[2])

        # define the objective output for our function
        output = 424242

        # calculate the output
        r = self.f(x, y, z)

        # calc error and fitness
        error = abs(r-output)

        # if error is small fitness is high
        # make sure that if error can not be 0 :)
        fitness = 1.0/(error + 0.00000000000000001)

        # print(f'error: {error} fitness {fitness}')
        return fitness
    


from enum import IntEnum
from collections import deque
from circuit import circuit, op


class Logical_circuit_experiment:
    """
    Class defining an logical circuit experiment.
    It will evolve a logical circuit that produce the desired outputs for the given inputs
    """

    def __init__(self, exp_inputs, exp_expected_outputs, num_logical_inputs, num_logical_outputs, num_logical_gates):
        self.exp_inputs = exp_inputs
        self.expected_outputs = exp_expected_outputs
        self.environment = None

        # logical circuit properties
        self.num_logical_inputs = num_logical_inputs
        self.num_logical_outputs = num_logical_outputs
        self.num_logical_gates = num_logical_gates

    def conduct(self, gene):
        """
        Conducts the experiment with the given gene
        Returns the fitness value.
        Parameters
        ----------
        gene: numpy.array(dtype=numpy.bool_)
        """
        # 1. form a gate representation from the gene string we can simpler work with
        gate_structs = self.form_gate_structure(gene)
        
        # 2. construct logic circuit
        logic_circuit = self.construct_logic_circuit(gate_structs)
        
        # check if the circuit was formed or there was an error due to inappropriate formed gates
        # e.g. loops in the circuit or gates with a self reference
        # if so return a zero fitness
        if logic_circuit == -1:
            return 0
        
        # 3. the circuit works correctly, calculate fitness
    
        # prune gates that are not needed, i.e. have dead ends, outputs can not be reached
        logic_circuit.prune_and_topological_sort_stable()
        used_gates = logic_circuit.count()
        num_gates_full = self.num_logical_gates + self.num_logical_inputs + self.num_logical_outputs
        # let this count really less since the main goal is to have a circuit that works not that is small
        fitness_used_gates = (num_gates_full/used_gates)/num_gates_full

        # check circuit outputs against expected outputs
        circuit_out = [list(logic_circuit.evaluate(bs)) for bs in self.exp_inputs]
        # input vs output
        e_full = self.error_full_bits(circuit_out, self.expected_outputs)
        # single bit comparison
        e_bits = self.error_single_bits(circuit_out, self.expected_outputs)
        
        # calc fitness from weighted errors
        fitness = 1/((1*e_full) + (0.02*e_bits) + 0.000000000000001)
        # addd other fitness measures
        fitness += fitness_used_gates

        return fitness

    def form_gate_structure(self, gene):
        """Transforms gene in a list of gate_structures for easy handling"""
        def walking_on_bits(bits, gate_id, adressing_mode, direction, length):
            """will walk on a list of elements in a direction (back,forth) starting from the beginnind or gate_id by length"""
            # from a current_position or from beginning?
            if bool(adressing_mode):
                # change the walking direction if desired
                if bool(direction):
                    return bits[(gate_id - length) % len(bits)]
                else:
                    return bits[(gate_id + length) % len(bits)]
            else:
                return bits[length % len(bits)]
        # -------------------------
        #       general
        # -------------------------
        # lengths of the gene blocks (overall, in, gates, out)
        gene_part_lens = self.needed_gene_length()

        # save the gate structures
        num_input_and_logic_and_output_gates = self.num_logical_inputs + \
            self.num_logical_gates+self.num_logical_outputs
        # ids for the gates on the gene string
        gate_ids = np.arange(
            0, num_input_and_logic_and_output_gates, dtype=np.int_)
        # holds the gate_structure objects
        gate_structs = np.arange(
            0, num_input_and_logic_and_output_gates, dtype=np.object_)

        # -------------------------
        #    add the logic gates
        # -------------------------
        # get the logic gate part of the genom
        gene_gate_coding_bits = gene[gene_part_lens[1]
            :gene_part_lens[0]-gene_part_lens[3]]

        # how long is one gate block in bits?
        gate_coding_len = int(gene_part_lens[2]/self.num_logical_gates)

        # extract each gate and give it the correct id
        gs = 0  # index marker
        for gate_id in range(self.num_logical_inputs, self.num_logical_inputs + self.num_logical_gates):
            g_bits = gene_gate_coding_bits[gs:gs+gate_coding_len]

            # function
            # --------
            function_bits = g_bits[0:3]
            func = self.gate_function(self.bool2int(function_bits))  # .name

            # inputs
            # -------
            # coding len of one input (3 bits for function and *2 inputs)
            input_coding_len = int((gate_coding_len - 3) / 2)

            # input 0
            # -------
            in_0_bits = g_bits[3:3+input_coding_len]
            #print(f'gate {gate_id} in0bits {in_0_bits}')
            direction = in_0_bits[0]
            adressing_mode = in_0_bits[1]
            length = self.bool2int(in_0_bits[2:])

            # do not connect to output gates!
            ids_tmp = gate_ids[0:num_input_and_logic_and_output_gates -
                               self.num_logical_outputs]

            # # get the index
            # if bool(adressing_mode):
            #     # change the walking direction if desired
            #     if bool(direction):
            #         in_0_gate_index = ids_tmp[gate_id - length % len(ids_tmp)]
            #     else:
            #         in_0_gate_index = ids_tmp[gate_id + length % len(ids_tmp)]
            # else:
            #     in_0_gate_index = ids_tmp[length % len(ids_tmp)]
            in_0_gate_index = walking_on_bits(
                ids_tmp, gate_id, adressing_mode, direction, length)
            # print(f'connectgate: {gate_id} in_0 to {in_0_gate_index}')

            # input 1
            # -------
            in_1_bits = g_bits[3+input_coding_len:]
            #print(f'gate {gate_id} in1bits {in_1_bits}')
            direction = in_1_bits[0]
            adressing_mode = in_1_bits[1]
            length = self.bool2int(in_1_bits[2:])

            # do not connect to output gates!
            ids_tmp = gate_ids[0:num_input_and_logic_and_output_gates -
                               self.num_logical_outputs]

            # # get the index
            # if bool(adressing_mode):
            #     # change the walking direction if desired
            #     if bool(direction):
            #         in_1_gate_index = ids_tmp[gate_id - length % len(ids_tmp)]
            #     else:
            #         in_1_gate_index = ids_tmp[gate_id + length % len(ids_tmp)]
            # else:
            #     in_1_gate_index = ids_tmp[length % len(ids_tmp)]
            in_1_gate_index = walking_on_bits(
                ids_tmp, gate_id, adressing_mode, direction, length)
            #print(f'connectgate: {gate_id} in_1 to {in_1_gate_index}')

            # construct the gate struct and save it to the index
            gate = self.gate_struct(
                gate_id, func, in_0_gate_index, in_1_gate_index)
            gate_structs[gate_id] = gate

            # update gate start index marker
            gs += gate_coding_len

            # debug
            # print('deboult for gate ', gate_id)
            # print('gbits',g_bits)
            # print('in0bits',in_0_bits)
            # print('funcbits',function_bits, ' function',func)
            # print(in_0_bits, length)
            # print(in_1_bits, length)
            # print(gate_structs)
            # break

        # --------------------
        # add the input gates
        # --------------------
        # extract the input gate coding bits from the gene
        gene_input_coding_bits = gene[0:gene_part_lens[1]]
        # coding len for one input
        input_coding_len = int(
            len(gene_input_coding_bits)/self.num_logical_inputs)

        gs = 0  # gene counter
        # construct the gates
        for gate_id in range(self.num_logical_inputs):
            gate = self.gate_struct(gate_id, self.gate_function.IN, None, None)
            gate_structs[gate_id] = gate

            # connect the output of this input gate to the logic gates by updating the logic gate inputs
            coding_bits = gene_input_coding_bits[gs:gs+input_coding_len]
            gate_indexes = np.split(coding_bits, len(coding_bits)/2)

            # now connext all the indexes
            # here gate_indexes[0] mean gate_0 from all logical gates
            for g_id, inputs in zip(range(self.num_logical_inputs, self.num_logical_inputs + self.num_logical_gates), gate_indexes):
                # connect the gates
                #print(gate_id, g_id, inputs)
                if inputs[0]:
                    gate_structs[g_id].in_0 = gate_id
                if inputs[1]:
                    # check if we have only one port input gate, then clamp the input to that port
                    if gate_structs[g_id].function == self.gate_function.NOT or gate_structs[g_id].function == self.gate_function.BUFF:
                        gate_structs[g_id].in_0 = gate_id
                    else:
                        gate_structs[g_id].in_1 = gate_id
            # update gene counter
            gs += len(coding_bits)

        # --------------------
        # add the output gates
        # --------------------
        # coding bits for the output
        gene_output_coding_bits = gene[len(gene)-gene_part_lens[3]:]
        output_coding_len = int(
            len(gene_output_coding_bits)/self.num_logical_outputs)
        gs = 0  # gene counter

        out_codings = np.split(gene_output_coding_bits,
                               self.num_logical_outputs)

        for out_id, bits in zip(range(num_input_and_logic_and_output_gates-self.num_logical_outputs, num_input_and_logic_and_output_gates), out_codings):
            # how to walk on the indexes?
            direction = bits[0]
            length = self.bool2int(bits[1:])

            # do not connect to input or output gates!
            ids_tmp = gate_ids[self.num_logical_inputs:
                               num_input_and_logic_and_output_gates-self.num_logical_outputs]
            # direction of walk
            if bool(direction):
                ids_tmp = ids_tmp[::-1]
            # get the connection index
            receives_input_from_gate = ids_tmp[length % len(ids_tmp)]

            # gate_struct
            gate = self.gate_struct(
                out_id, self.gate_function.OUT, receives_input_from_gate, None)
            gate_structs[out_id] = gate

        return gate_structs
    
    def construct_logic_circuit(self, gate_structs):
        """Construct the logic circuit from a list of logic structures, i.e. call self.form_gate_structure(gene) to get them.
        synthesize all gates in the following way:
        check if all needed inputs are already synthesized, if so synthesize the gate
        if not walk along the inputs until a gate that is synthesizable is found.
        Synthesize this gate until all gates are synthesyzed.
        """
        # synthesized gate ids
        synthesized = []

        # synthesized gate objects
        gate_instances = np.ones(len(gate_structs), dtype=np.object_)
        gate_instances = [n for n in range(len(gate_structs))]

        # circuit library object
        logical_circuit = circuit()

        # to be synthesized stack (linked list)
        to_be_synthesized = deque()

        for gate in gate_structs:
            # check if we already synthesized this gate
            if gate.gate_id in synthesized:
                continue
            # synthesize the gate and all its depedencies
            to_be_synthesized.append(gate)
            # while we have gates to be synthesized
            while to_be_synthesized:
                # get the next to be synthesized gate
                tbsg = to_be_synthesized.pop()
                
                # if we have a self reference we can not construct the gate as the logic lib does not allow
                # as such this circuit is unusable and has an fitness 0
                # there are more clever ways like only construct parts of the circuit that does not depend
                # on the self reference gate but for now we just skip that.
                # IMPLEMENTME:
                # think of if it makes sense to just construct the parts of the circuit that do not depend on
                # this gate instead of rendering the whole solution useless.
                if tbsg.in_0 == tbsg.gate_id or tbsg.in_1 == tbsg.gate_id:
                    return -1
                
                # check if we can synthesize the gate
                if (
                    (tbsg.function == self.gate_function.IN)
                    or (
                        (
                            tbsg.function == self.gate_function.OUT
                            or tbsg.function == self.gate_function.BUFF
                            or tbsg.function == self.gate_function.NOT
                        )
                        and tbsg.in_0 in synthesized
                    )
                    or (tbsg.in_0 in synthesized and tbsg.in_1 in synthesized)
                ):
                    # construct the gate
                    if tbsg.function == self.gate_function.IN:
                        g_new = logical_circuit.gate(op.id_, is_input=True)

                    if tbsg.function == self.gate_function.OUT:
                        g_new = logical_circuit.gate(op.id_, [gate_instances[tbsg.in_0]], is_output=True)

                    # since logic does not support multic clock circuits we have no buffer
                    # instead use the ID function (just put out what comes in, like a wire :P)
                    if tbsg.function == self.gate_function.BUFF:
                        g_new = logical_circuit.gate(op.id_, [gate_instances[tbsg.in_0]])

                    if tbsg.function == self.gate_function.NOT:
                        g_new = logical_circuit.gate(op.not_, [gate_instances[tbsg.in_0]])

                    if tbsg.function == self.gate_function.AND:
                        g_new = logical_circuit.gate(
                            op.and_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.OR:
                        g_new = logical_circuit.gate(
                            op.or_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.XOR:
                        g_new = logical_circuit.gate(
                            op.xor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.NAND:
                        g_new = logical_circuit.gate(
                            op.nand_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.NOR:
                        g_new = logical_circuit.gate(
                            op.nor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.XNOR:
                        g_new = logical_circuit.gate(
                            op.xnor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    # add gate references to our lists
                    synthesized.append(tbsg.gate_id)
                    gate_instances[tbsg.gate_id] = g_new
                # if we can not synthesize the gate add its inputs to the to_be_synthesized list and
                # try to synthesize them first
                else:
                    # gate was not synthesizeable we need to push it back to the stack
                    to_be_synthesized.append(tbsg)
                    #print(tbsg)

                    # check both inputs
                    # in_-
                    # already synthesized?
                    if tbsg.in_0 not in synthesized:
                        # check if we have connection loop which can not be handled!
                        # logic does not support these circles.
                        # however for real hardware simulators that are not one clock tick functions this is common
                        if gate_structs[tbsg.in_0] in to_be_synthesized:
                            return -1
                        # add the gate
                        to_be_synthesized.append(gate_structs[tbsg.in_0])

                    # in_1
                    # check if we need to add since out, buff, not gates do not have two inputs
                    if (
                        tbsg.in_1 not in synthesized
                        and tbsg.function != self.gate_function.OUT
                        and tbsg.function != self.gate_function.BUFF
                        and tbsg.function != self.gate_function.NOT
                    ):
                        # check for connection loop
                        if gate_structs[tbsg.in_1] in to_be_synthesized:
                            return -1
                        # add
                        to_be_synthesized.append(gate_structs[tbsg.in_1])
        # return result
        return logical_circuit
    
    def error_full_bits(self, x, y):
        """
        Calculates error based how many fully correct outputs where given
        returns counts where x != y
        """
        e = 0
        a = np.array(x)
        b = np.array(y)
        c = a == b
        for i in c:
            # correct output?
            if False in i:
                e += 1
        return e
    
    def error_single_bits(self, x, y):
        """
        Calculates the fitness based how many correct bits in each output
        """
        e = 0
        a = np.array(x)
        b = np.array(y)
        for i_a, i_b in zip(a, b):
            c = i_a - i_b
            e += np.count_nonzero(c)/len(i_a)
        return e


    def needed_gene_length(self):
        """
        Return the lenght of the gene needed to encode all logical gates
        overall_lenght, inputcodinglen, gatescodinglen, outputcodinglen
        """
        gene_bits_needed = 0

        # for the input gates
        # each input gate has two bits (first or second input of that gate) for representing its connection to a gate
        # (00 10 01 00) would connect the input to the first input of gate 1  and the second input of gate 2, not to gate 0 and 3
        input_gate_coding_bits = self.num_logical_inputs * self.num_logical_gates * 2

        # gates
        # each gate has 3 bits for its gate function
        # buffer, not, and, or, xor, nand, nor, xnor
        # and stores two adresses for its inputs.
        # the adresses are stored as in thompson  (96 university of sussex) (direction, adressing mode, length)
        # where bits needed are (1, 1, len(bin(num_input_gates + num_gates)))
        # this avoids that a gate can be connected to a gate that does not exist by walking on the list of gates a certain len and start from the beginning if the end is reached.
        # In summary each gate has:
        # (gate_function, input_0, input_1)
        gate_input_coding_bits = (
            1 + 1 + len(bin(self.num_logical_inputs +
                        self.num_logical_gates)) - 2
        )  # -2 since bin() will be 0bxyz
        gate_coding_bits = self.num_logical_gates * (
            3 + gate_input_coding_bits + gate_input_coding_bits
        )

        # outputs
        # each output is defined as
        # (direction, adressing_mode, length)
        # which means the connecting gate is defined by walking in the direction in positive or negative(adressing_mode) lenght, on a list on all gates existend in the circuit.
        gate_output_coding_bits = self.num_logical_outputs * (
            1 + len(bin(self.num_logical_gates)) - 2
        )

        # final gene length
        return (
            input_gate_coding_bits + gate_coding_bits + gate_output_coding_bits,
            input_gate_coding_bits,
            gate_coding_bits,
            gate_output_coding_bits,
        )

    # used from here
    # https://stackoverflow.com/questions/15505514/binary-numpy-array-to-list-of-integers
    def bool2int(self, x):
        x = x[::-1]
        r = 0
        for i, b in enumerate(x):
            if b:
                r += b << i
        return r

    class gate_struct:
        """Class representing an abstract logic gate"""

        def __init__(self, gate_id, function, in_0, in_1):
            self.function = function
            self.in_0 = in_0
            self.in_1 = in_1
            self.gate_id = gate_id

        def __str__(self):
            s = f'Gate ID: {self.gate_id} '
            s += f'Func: {self.function.name} '
            s += f'in_0: {self.in_0} '
            s += f'in_1: {self.in_1} '
            return s

    class gate_function(IntEnum):
        """Specifie a logical gate function coding"""

        # Logic gates
        BUFF = 0
        NOT = 1
        AND = 2
        OR = 3
        XOR = 4
        NAND = 5
        NOR = 6
        XNOR = 7

        # special gates
        IN = 8
        OUT = 9
        
        
class Logical_circuit_experiment_no_explicit_input_coding(Experiment):
    """
    Class defining an logical circuit experiment.
    It will evolve a logical circuit that produce the desired outputs for the given inputs
    """

    def __init__(self, exp_inputs, exp_expected_outputs, num_logical_inputs, num_logical_outputs, num_logical_gates):
        self.exp_inputs = exp_inputs
        self.expected_outputs = exp_expected_outputs
        self.environment = None

        # logical circuit properties
        self.num_logical_inputs = num_logical_inputs
        self.num_logical_outputs = num_logical_outputs
        self.num_logical_gates = num_logical_gates

    def conduct(self, gene):
        """
        Conducts the experiment with the given gene
        Returns the fitness value.
        Parameters
        ----------
        gene: numpy.array(dtype=numpy.bool_)
        """
        # 1. form a gate representation from the gene string we can simpler work with
        gate_structs = self.form_gate_structure(gene)
        
        # 2. construct logic circuit
        logic_circuit = self.construct_logic_circuit(gate_structs)
        
        # check if the circuit was formed or there was an error due to inappropriate formed gates
        # e.g. loops in the circuit or gates with a self reference
        # if so return a zero fitness
        if logic_circuit == -1:
            return 0
        
        # 3. the circuit works correctly, calculate fitness
    
        # prune gates that are not needed, i.e. have dead ends, outputs can not be reached
        logic_circuit.prune_and_topological_sort_stable()
        used_gates = logic_circuit.count()
        num_gates_full = self.num_logical_gates + self.num_logical_inputs + self.num_logical_outputs
        # let this count really less since the main goal is to have a circuit that works not that is small
        fitness_used_gates = (num_gates_full/used_gates)/num_gates_full

        # check circuit outputs against expected outputs
        circuit_out = [list(logic_circuit.evaluate(bs)) for bs in self.exp_inputs]
        # input vs output
        e_full = self.error_full_bits(circuit_out, self.expected_outputs)
        # single bit comparison
        e_bits = self.error_single_bits(circuit_out, self.expected_outputs)
        
        # calc fitness from weighted errors
        fitness = 1/((1*e_full) + (0.02*e_bits) + 0.000000000000001)
        # addd other fitness measures
        fitness += fitness_used_gates

        return fitness

    def form_gate_structure(self, gene):
        """Transforms gene in a list of gate_structures for easy handling"""
        def walking_on_bits(bits, gate_id, adressing_mode, direction, length):
            """will walk on a list of elements in a direction (back,forth) starting from the beginnind or gate_id by length"""
            # from a current_position or from beginning?
            if bool(adressing_mode):
                # change the walking direction if desired
                if bool(direction):
                    return bits[(gate_id - length) % len(bits)]
                else:
                    return bits[(gate_id + length) % len(bits)]
            else:
                return bits[length % len(bits)]
        # -------------------------
        #       general
        # -------------------------
        # lengths of the gene blocks (overall, in, gates, out)
        gene_part_lens = self.needed_gene_length()

        # save the gate structures
        num_input_and_logic_and_output_gates = self.num_logical_inputs + \
            self.num_logical_gates+self.num_logical_outputs
        # ids for the gates on the gene string
        gate_ids = np.arange(
            0, num_input_and_logic_and_output_gates, dtype=np.int_)
        # holds the gate_structure objects
        gate_structs = np.arange(
            0, num_input_and_logic_and_output_gates, dtype=np.object_)

        # -------------------------
        #    add the logic gates
        # -------------------------
        # get the logic gate part of the genom
        gene_gate_coding_bits = gene[gene_part_lens[1]
            :gene_part_lens[0]-gene_part_lens[3]]

        # how long is one gate block in bits?
        gate_coding_len = int(gene_part_lens[2]/self.num_logical_gates)

        # extract each gate and give it the correct id
        gs = 0  # index marker
        for gate_id in range(self.num_logical_inputs, self.num_logical_inputs + self.num_logical_gates):
            g_bits = gene_gate_coding_bits[gs:gs+gate_coding_len]

            # function
            # --------
            function_bits = g_bits[0:3]
            func = self.gate_function(self.bool2int(function_bits))  # .name

            # inputs
            # -------
            # coding len of one input (3 bits for function and *2 inputs)
            input_coding_len = int((gate_coding_len - 3) / 2)

            # input 0
            # -------
            in_0_bits = g_bits[3:3+input_coding_len]
            #print(f'gate {gate_id} in0bits {in_0_bits}')
            direction = in_0_bits[0]
            adressing_mode = in_0_bits[1]
            length = self.bool2int(in_0_bits[2:])
            # do not connect to output gates!
            ids_tmp = gate_ids[0:num_input_and_logic_and_output_gates -
                               self.num_logical_outputs]
            # # get the index
            in_0_gate_index = walking_on_bits(
                ids_tmp, gate_id, adressing_mode, direction, length)
            # print(f'connectgate: {gate_id} in_0 to {in_0_gate_index}')

            # input 1
            # -------
            in_1_bits = g_bits[3+input_coding_len:]
            #print(f'gate {gate_id} in1bits {in_1_bits}')
            direction = in_1_bits[0]
            adressing_mode = in_1_bits[1]
            length = self.bool2int(in_1_bits[2:])
            # do not connect to output gates!
            ids_tmp = gate_ids[0:num_input_and_logic_and_output_gates -
                               self.num_logical_outputs]
            # # get the index
            in_1_gate_index = walking_on_bits(
                ids_tmp, gate_id, adressing_mode, direction, length)
            #print(f'connectgate: {gate_id} in_1 to {in_1_gate_index}')
            
            # Construct:
            # --------------
            # construct the gate struct and save it to the index
            gate = self.gate_struct(
                gate_id, func, in_0_gate_index, in_1_gate_index)
            gate_structs[gate_id] = gate

            # update gate start index marker
            gs += gate_coding_len

            # debug
            # print('deboult for gate ', gate_id)
            # print('gbits',g_bits)
            # print('in0bits',in_0_bits)
            # print('funcbits',function_bits, ' function',func)
            # print(in_0_bits, length)
            # print(in_1_bits, length)
            # print(gate_structs)
            # break

        # --------------------
        # add the input gates
        # --------------------
        # inputs have just to be added
        # construct the gates
        for gate_id in range(self.num_logical_inputs):
            gate = self.gate_struct(gate_id, self.gate_function.IN, None, None)
            gate_structs[gate_id] = gate
            
        # --------------------
        # add the output gates
        # --------------------
        # coding bits for the output
        gene_output_coding_bits = gene[len(gene)-gene_part_lens[3]:]
        output_coding_len = int(
            len(gene_output_coding_bits)/self.num_logical_outputs)
        gs = 0  # gene counter

        out_codings = np.split(gene_output_coding_bits,
                               self.num_logical_outputs)

        for out_id, bits in zip(range(num_input_and_logic_and_output_gates-self.num_logical_outputs, num_input_and_logic_and_output_gates), out_codings):
            # how to walk on the indexes?
            direction = bits[0]
            length = self.bool2int(bits[1:])

            # do not connect to input or output gates!
            ids_tmp = gate_ids[self.num_logical_inputs:
                               num_input_and_logic_and_output_gates-self.num_logical_outputs]
            # direction of walk
            if bool(direction):
                ids_tmp = ids_tmp[::-1]
            # get the connection index
            receives_input_from_gate = ids_tmp[length % len(ids_tmp)]

            # gate_struct
            gate = self.gate_struct(
                out_id, self.gate_function.OUT, receives_input_from_gate, None)
            gate_structs[out_id] = gate

        return gate_structs
    
    def construct_logic_circuit(self, gate_structs):
        """Construct the logic circuit from a list of logic structures, i.e. call self.form_gate_structure(gene) to get them.
        synthesize all gates in the following way:
        check if all needed inputs are already synthesized, if so synthesize the gate
        if not walk along the inputs until a gate that is synthesizable is found.
        Synthesize this gate until all gates are synthesyzed.
        """
        # synthesized gate ids
        synthesized = []

        # synthesized gate objects
        gate_instances = np.ones(len(gate_structs), dtype=np.object_)
        gate_instances = [n for n in range(len(gate_structs))]

        # circuit library object
        logical_circuit = circuit()

        # to be synthesized stack (linked list)
        to_be_synthesized = deque()

        for gate in gate_structs:
            # check if we already synthesized this gate
            if gate.gate_id in synthesized:
                continue
            # synthesize the gate and all its depedencies
            to_be_synthesized.append(gate)
            # while we have gates to be synthesized
            while to_be_synthesized:
                # get the next to be synthesized gate
                tbsg = to_be_synthesized.pop()
                
                # if we have a self reference we can not construct the gate as the logic lib does not allow
                # as such this circuit is unusable and has an fitness 0
                # there are more clever ways like only construct parts of the circuit that does not depend
                # on the self reference gate but for now we just skip that.
                # IMPLEMENTME:
                # think of if it makes sense to just construct the parts of the circuit that do not depend on
                # this gate instead of rendering the whole solution useless.
                if tbsg.in_0 == tbsg.gate_id or tbsg.in_1 == tbsg.gate_id:
                    return -1
                
                # check if we can synthesize the gate
                if (
                    (tbsg.function == self.gate_function.IN)
                    or (
                        (
                            tbsg.function == self.gate_function.OUT
                            or tbsg.function == self.gate_function.BUFF
                            or tbsg.function == self.gate_function.NOT
                        )
                        and tbsg.in_0 in synthesized
                    )
                    or (tbsg.in_0 in synthesized and tbsg.in_1 in synthesized)
                ):
                    # construct the gate
                    if tbsg.function == self.gate_function.IN:
                        g_new = logical_circuit.gate(op.id_, is_input=True)

                    if tbsg.function == self.gate_function.OUT:
                        g_new = logical_circuit.gate(op.id_, [gate_instances[tbsg.in_0]], is_output=True)

                    # since logic does not support multic clock circuits we have no buffer
                    # instead use the ID function (just put out what comes in, like a wire :P)
                    if tbsg.function == self.gate_function.BUFF:
                        g_new = logical_circuit.gate(op.id_, [gate_instances[tbsg.in_0]])

                    if tbsg.function == self.gate_function.NOT:
                        g_new = logical_circuit.gate(op.not_, [gate_instances[tbsg.in_0]])

                    if tbsg.function == self.gate_function.AND:
                        g_new = logical_circuit.gate(
                            op.and_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.OR:
                        g_new = logical_circuit.gate(
                            op.or_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.XOR:
                        g_new = logical_circuit.gate(
                            op.xor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.NAND:
                        g_new = logical_circuit.gate(
                            op.nand_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.NOR:
                        g_new = logical_circuit.gate(
                            op.nor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    if tbsg.function == self.gate_function.XNOR:
                        g_new = logical_circuit.gate(
                            op.xnor_, [gate_instances[tbsg.in_0], gate_instances[tbsg.in_1]]
                        )

                    # add gate references to our lists
                    synthesized.append(tbsg.gate_id)
                    gate_instances[tbsg.gate_id] = g_new
                # if we can not synthesize the gate add its inputs to the to_be_synthesized list and
                # try to synthesize them first
                else:
                    # gate was not synthesizeable we need to push it back to the stack
                    to_be_synthesized.append(tbsg)
                    #print(tbsg)

                    # check both inputs
                    # in_-
                    # already synthesized?
                    if tbsg.in_0 not in synthesized:
                        # check if we have connection loop which can not be handled!
                        # logic does not support these circles.
                        # however for real hardware simulators that are not one clock tick functions this is common
                        if gate_structs[tbsg.in_0] in to_be_synthesized:
                            return -1
                        # add the gate
                        to_be_synthesized.append(gate_structs[tbsg.in_0])

                    # in_1
                    # check if we need to add since out, buff, not gates do not have two inputs
                    if (
                        tbsg.in_1 not in synthesized
                        and tbsg.function != self.gate_function.OUT
                        and tbsg.function != self.gate_function.BUFF
                        and tbsg.function != self.gate_function.NOT
                    ):
                        # check for connection loop
                        if gate_structs[tbsg.in_1] in to_be_synthesized:
                            return -1
                        # add
                        to_be_synthesized.append(gate_structs[tbsg.in_1])
        # return result
        return logical_circuit
    
    def error_full_bits(self, x, y):
        """
        Calculates error based how many fully correct outputs where given
        returns counts where x != y
        """
        e = 0
        a = np.array(x)
        b = np.array(y)
        c = a == b
        for i in c:
            # correct output?
            if False in i:
                e += 1
        return e
    
    def error_single_bits(self, x, y):
        """
        Calculates the fitness based how many correct bits in each output
        """
        e = 0
        a = np.array(x)
        b = np.array(y)
        for i_a, i_b in zip(a, b):
            c = i_a - i_b
            e += np.count_nonzero(c)/len(i_a)
        return e


    def needed_gene_length(self):
        """
        Return the lenght of the gene needed to encode all logical gates
        overall_lenght, inputcodinglen, gatescodinglen, outputcodinglen
        """
        gene_bits_needed = 0

        # input gates are not code in the genome
        input_gate_coding_bits = 0

        # gates
        # each gate has 3 bits for its gate function
        # buffer, not, and, or, xor, nand, nor, xnor
        # and stores two adresses for its inputs.
        # the adresses are stored as in thompson  (96 university of sussex) (direction, adressing mode, length)
        # where bits needed are (1, 1, len(bin(num_input_gates + num_gates)))
        # this avoids that a gate can be connected to a gate that does not exist by walking on the list of gates a certain len and start from the beginning if the end is reached.
        # In summary each gate has:
        # (gate_function, input_0, input_1)
        gate_input_coding_bits = (
            1 + 1 + len(bin(self.num_logical_inputs +
                        self.num_logical_gates)) - 2
        )  # -2 since bin() will be 0bxyz
        gate_coding_bits = self.num_logical_gates * (
            3 + gate_input_coding_bits + gate_input_coding_bits
        )

        # outputs
        # each output is defined as
        # (direction, adressing_mode, length)
        # which means the connecting gate is defined by walking in the direction in positive or negative(adressing_mode) lenght, on a list on all gates existend in the circuit.
        gate_output_coding_bits = self.num_logical_outputs * (
            1 + len(bin(self.num_logical_gates)) - 2
        )

        # final gene length
        return (
            input_gate_coding_bits + gate_coding_bits + gate_output_coding_bits,
            input_gate_coding_bits,
            gate_coding_bits,
            gate_output_coding_bits,
        )

    # used from here
    # https://stackoverflow.com/questions/15505514/binary-numpy-array-to-list-of-integers
    def bool2int(self, x):
        x = x[::-1]
        r = 0
        for i, b in enumerate(x):
            if b:
                r += b << i
        return r

    class gate_struct:
        """Class representing an abstract logic gate"""

        def __init__(self, gate_id, function, in_0, in_1):
            self.function = function
            self.in_0 = in_0
            self.in_1 = in_1
            self.gate_id = gate_id

        def __str__(self):
            s = f'Gate ID: {self.gate_id} '
            s += f'Func: {self.function.name} '
            s += f'in_0: {self.in_0} '
            s += f'in_1: {self.in_1} '
            return s

    class gate_function(IntEnum):
        """Specifie a logical gate function coding"""

        # Logic gates
        BUFF = 0
        NOT = 1
        AND = 2
        OR = 3
        XOR = 4
        NAND = 5
        NOR = 6
        XNOR = 7

        # special gates
        IN = 8
        OUT = 9
