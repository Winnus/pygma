from components.genetic_operators import Mutation_operator, Single_point_crossover_operator, Removal_operator
from components.evolutionary_phase import Simple_evolutionary_phase
from components.experiment import Function_optimisation_experiment


# -----------------------------
# Genetic operator definition
# -----------------------------
# Genetic operators can be used for:
# Populations
# Genetic Phases

OP_MUTATION = Mutation_operator(0.06, 3)
OP_CROSSOVER = Single_point_crossover_operator(3)
OP_REMOVAL = Removal_operator(6)



# -----------------------------
# Phase definition
# -----------------------------
# Genetic phases allowing for evolution of individuals in
# changing environments or conditions.

# phase 0
# phase starting operators (applied at phase start)
PHASE0_SOP = [OP_MUTATION, OP_MUTATION]

# phase Experiment (conducted on individuals)
PHASE0_EXPERIMENT = Function_optimisation_experiment()

# phase definition
PHASE0 = Simple_evolutionary_phase(
    'Phase_0', PHASE0_SOP, PHASE0_EXPERIMENT)

# phase 1


# -----------------------------
# GA Configuration parameters
# -----------------------------
# These are all general parameters for the Algorithm
# in form of a dictionary
CONFIG_FUNC_OP = {
    # -------------
    # Processes
    # --------------
    # use mpi parallelization
    # If use_mpi4py_futures=False start with:
    # mpiexec -n 3 python PyGMA.py
    # or use threads if not specified via slurm
    # mpiexec -n 3 --use-hwthread-cpus python PyGMA.py
    'use_mpi': True,
    
    # use mpi4py.futures
    # this will dynamically spawn workers.
    # NOTE: you have to execute the programm in this manner:
    # mpiexec -n 3 python -m mpi4py.futures PyGMA.py
    'use_mpi4py_futures': False,

    # if mpi=False, how many local processes to use
    # Note if > 1 it will spawn additonal processes that independently
    # solves the experiment. Spawning takes time (memcopys etc)
    # if the experiment is very simple having only one process
    # handling everyting might be faster.
    # start programm with:
    # python PyGMA.py
    'num_local_processes': 1,


    # -------------
    # Populations
    # --------------
    # how many island populations to use
    # is defined by the genetic operators stack lenght
    # used to recombine/produce, mutate, extend...
    'genetic_operator_stack': [
        # pop 0
        [OP_REMOVAL, OP_CROSSOVER, OP_CROSSOVER],
        # pop 1
        [OP_REMOVAL, OP_CROSSOVER, OP_MUTATION],
        # pop 2
        [OP_REMOVAL, OP_CROSSOVER, OP_MUTATION]
    ],

    # how many island populations to use
    # 'num_populations': 3,

    # how many individuals per population
    'num_individuals': 30,

    # genome lenght for each individual
    'genome_length': 60,

    # init populations from defined gene strings
    # this will ovveride:
    # num_populations, num_individuals, genome_length
    'use_predefined_defined_genes': False,
    'predefined_genes': [
        # pop 0
        [[0, 0, 1], [0, 1, 1]],
        # pop 1
        [[0, 1, 1], [1, 1, 1]]
    ],

    # -------------
    # Evolutinary Phases
    # --------------
    # defining the phases that will be sequantially evolved
    'evolutionary_phases': [
        PHASE0, PHASE0
    ]

}

