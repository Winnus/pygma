from components.genetic_operators import Mutation_operator, Single_point_crossover_operator, Removal_operator, Set_zeros
from components.evolutionary_phase import Simple_evolutionary_phase
from components.experiment import Logical_circuit_experiment
import numpy as np

# ------------------------------------
#          helper functions
# ------------------------------------
def n_bit_multiplication_inout(n):
    """
    Generate all possible bit combinations for multipling two n bit numbers.
    Inputs are not separated in the list of list.
    meaning E.G. if n = 2 
    and num_1 = 01, num_2 = 10 then (meaning 1*2=2)
    inputs = [[0,1,1,0]]
    outputs = [[1,0]]
    """
    # variables
    inputs = []
    outputs = []

    # mult is commutative, hence to loops...
    for a in range(2**n):
        for b in range(2**n):
            # calulate and transfer to bit arrays
            c = a*b
            a_bit = np.array(list(bin(a)[2:]), dtype=np.int64) # scrub 0b...
            b_bit = np.array(list(bin(b)[2:]), dtype=np.int64)
            c_bit = np.array(list(bin(c)[2:]), dtype=np.int64)

            #print(a_bit)
            # now we have scrubbed bits but we need the
            # full length vector
            # create the full length bit vectors
            a_fbit = np.zeros(n, dtype=np.bool_)
            #print(a_fbit)
            a_fbit[np.arange(len(a_bit))] = a_bit[::-1]
            #print(a_fbit)
            a_fbit = a_fbit[::-1]
            #print(a_fbit)

            b_fbit = np.zeros(n, dtype=np.bool_)
            #print(b_fbit)
            b_fbit[np.arange(len(b_bit))] = b_bit[::-1]
            #print(b_fbit)
            b_fbit = b_fbit[::-1]
            #print(b_fbit)
            # i dunno how to convert it correctly for the circuit lib thats wy this workarround lifes happily among us
            inputs.append([int(num) for num in list(np.array(np.concatenate((a_fbit, b_fbit)), dtype=np.int64))])
            #print()

            # append output
            c_fbit = np.zeros(n+n, dtype=np.bool_)
            c_fbit[np.arange(len(c_bit))] = c_bit[::-1]
            c_fbit = c_fbit[::-1]
            outputs.append(list(np.array(c_fbit, dtype=np.int64)))
    return inputs, outputs


# -----------------------------
# Genetic operator definition
# -----------------------------
# Genetic operators can be used for:
# Populations
# Genetic Phases

OP_MUTATION = Mutation_operator(0.05, 45)
OP_CROSSOVER = Single_point_crossover_operator(45)
OP_REMOVAL = Removal_operator(90)



# -----------------------------
# Phase definition
# -----------------------------
# -----------------------------
# Phase definition
# -----------------------------
# defining the logical circuit experiment
# we will evolve a n bit adder.
# We will compute all possible combinations and check if we
# we can find a circuit that, given inputs produce the correct
# outputs, i.e. multiplies the numbers correctly

# how many bits num a and b have?
num_bits = 2

# calculating all possible combinations
exp_inputs, exp_expected_outputs = n_bit_multiplication_inout(num_bits)

num_logical_inputs = num_bits+num_bits # we have two numbers each having n bits
num_logical_outputs = num_bits+num_bits# we need to save the result
num_logical_gates = 42 # how many logic gates we want to use

PHASE0_EXPERIMENT = Logical_circuit_experiment(exp_inputs, exp_expected_outputs, num_logical_inputs, num_logical_outputs, num_logical_gates)

# phase starting operators (applied at phase start)
# these can for example change the genome length by applying
# an operator that would do so
OP_PS_SET_ZEROS = Set_zeros(np.arange(PHASE0_EXPERIMENT.needed_gene_length()[1]))
PHASE0_SOP = [OP_PS_SET_ZEROS]

# phase definition
PHASE0 = Simple_evolutionary_phase(
    'Phase_0', PHASE0_SOP, PHASE0_EXPERIMENT)

# phase 1


# -----------------------------
# GA Configuration parameters
# -----------------------------
# These are all general parameters for the Algorithm
# in form of a dictionary
CONFIG_CIRCUIT_EVOLVE = {
    # -------------
    # Processes
    # --------------
    # use mpi parallelization
    # If use_mpi4py_futures=False start with:
    # mpiexec -n 3 python PyGMA.py
    # or use threads if not specified via slurm
    # mpiexec -n 3 --use-hwthread-cpus python PyGMA.py
    'use_mpi': False,
    
    # use mpi4py.futures
    # this will dynamically spawn workers.
    # NOTE: you have to execute the programm in this manner:
    # mpiexec -n 3 python -m mpi4py.futures PyGMA.py
    'use_mpi4py_futures': False,

    # if mpi=False, how many local processes to use
    # Note if > 1 it will spawn additonal processes that independently
    # solves the experiment. Spawning takes time (memcopys etc)
    # if the experiment is very simple having only one process
    # handling everyting might be faster.
    # start programm with:
    # python PyGMA.py
    'num_local_processes': 1,


    # -------------
    # Populations
    # --------------
    # how many island populations to use
    # is defined by the genetic operators stack lenght
    # used to recombine/produce, mutate, extend...
    'genetic_operator_stack': [
        # pop 0
        [OP_REMOVAL, OP_CROSSOVER, OP_CROSSOVER],
        # pop 1
        [OP_REMOVAL, OP_MUTATION, OP_MUTATION],
        # pop 2
        [OP_REMOVAL, OP_CROSSOVER, OP_MUTATION]
    ],

    # how many island populations to use
    # 'num_populations': 3,

    # how many individuals per population
    'num_individuals': 94,

    # start genome lenght for each individual
    'genome_length': PHASE0_EXPERIMENT.needed_gene_length()[0],

    # init populations from defined gene strings
    # this will ovveride:
    # num_populations, num_individuals, genome_length
    'use_predefined_defined_genes': False,
    'predefined_genes': [
        # pop 0
        [[0, 0, 1], [0, 1, 1]],
        # pop 1
        [[0, 1, 1], [1, 1, 1]]
    ],

    # -------------
    # Evolutinary Phases
    # --------------
    # defining the phases that will be sequantially evolved
    'evolutionary_phases': [
        PHASE0
    ]

}

