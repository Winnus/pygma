from components.genetic_operators import Genetic_operator
from components.experiment import Experiment

# --------------
# Interface idea
# --------------


class Evolutionary_phase:
    """
    A phase in the evolutionary algorithm
    This class serves as informal interface and has to be extended
    """

    def __init__(self, name, phase_start_operators, experiment):
        self.name = name
        self.phase_start_operators: Genetic_operator = phase_start_operators
        self.experiment: Experiment = experiment

    def __str__(self):
        return self.name
    
    def completed(self, population_fitness: list, epochs: int) -> bool:
        """
         Called to check if the phase has reached its end. 
         You have to return true or false depending on if the phase is finished or not.
         For example:
         max_fitness = max(population_fitness)
         if max_fitness > 90.9:
            return True
         if epochs > 900:
            return True
         return False
        """
        raise NotImplementedError

    def initialize(self, populations: list) -> list:
        """
        Here one can combine fusion/recombine, change the genetic operators...
        returns a list with the populations that shall be used from now on.
        For example one could define new genetic operators and then apply 
        them by change the population.genetic_operator_stack = new_stack
        """
        # modifie popopulations
        return populations

# --------------
# Phases
# --------------


class Simple_evolutionary_phase(Evolutionary_phase):

    #    def __init__(self, phase_start_operators, experiment):
    #        super().__init__(phase_start_operators, experiment)

    def completed(self, population_fitness: list, epochs: int) -> bool:
        """
         Called to check if the phase has reached its end.
         
        """
        max_fitness = max(population_fitness)
        if max_fitness > 90.9:
            return True

        if epochs > 3000:
            return True

        return False
