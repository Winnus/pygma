import numpy as np

# -----------------
# Formal interface
# -----------------
# Write the idea into the docs that:
# for the operatos the idea is that they always append new childs
# into the population gene list.
# as such they are able to not remove genes that where made by other
# operators. If the population should be keep small apply the
# population shrinkage operator which will shrink the new_population
# in the first place so that now other operators can add new genes.
# The user knows how much he adds and as such the stack shall always
# come to no increase in the end.
# e.g. remove(4), crossover(2), add_random(4)...


class Genetic_operator():
    """
    This class will apply a genetic operator to a set of genes.
    """

    def operate(self, old_population, new_population) -> list:
        """
        Applys the genetic operator on the genes
        Parameters
        ----------
        old_population: list of numpy.array(dtype=numpy.bool_) genes
            genes before the operator stack is applied, sorted by their fitness
        new_population: list
            current state of modifications in operator stack
        Note that if this is not the first operator which is applied
        the actual fitness (the indexes, fitness_0 >= fitness_1) unclear.
        Fitness is always represented at the point where
        no operator was applied after an evaluation of the individuals.

        return: list
            new genes to be used, these have to be arrays of numpy booleans!
        """

        raise NotImplementedError

# -----------------
# Genetic Operators
# -----------------
class Set_zeros(Genetic_operator):
    """Sets the specified indexes to zero in a binary gene"""
    def __init__(self, indexes):
        self.indexes = indexes
        
    def operate(self, old_population, new_population):
        # from old set all the indexes to zero and make this the new pop
        new_population = []
        for gene in old_population:
            gene[self.indexes] = 0
            new_population.append(gene)
        return new_population

class Mutation_all_operator(Genetic_operator):
    """
    This class represents a binary mutation operator.
    It will produce n new individuals by mutating their genes with
    the propability defined by mutation rate
    """

    def __init__(self, mutation_rate, rng_seed=9):
        self.mutation_rate = mutation_rate
        self.rng = np.random.default_rng(rng_seed)

    def operate(self, old_population, new_population):
        """
        Apply mutation to all genes of the current op stack.
        """
        for gene in new_population:
            # mutation indexes
            # having at least one mutation
            mutations = int(self.mutation_rate*gene.size)
            if mutations == 0:
                mutations = 1
            indexes = self.rng.integers(low=0, high=gene.size, size=mutations)
            # flip the bits at indexes
            # note, ~ is like np.invert()
            gene[indexes] = ~gene[indexes]
        return new_population
    

class Mutation_operator(Genetic_operator):
    """
    This class represents a binary mutation operator.
    It will produce n new individuals by mutating their genes with
    the propability defined by mutation rate
    """

    def __init__(self, mutation_rate, new_individuals, rng_seed=9):
        self.mutation_rate = mutation_rate
        self.new_individuals = new_individuals
        self.rng = np.random.default_rng(rng_seed)

    def operate(self, old_population, new_population):
        """
        Apply mutation to all genes of the current op stack.
        """
        # take n individuals from the old_population (sorted by fitness) and mutate them
        for n in range(self.new_individuals):
            # FIXME:
            # take not always the first ones take by asceding propability from all ones
            gene = np.copy(old_population[0])
            gene_size = len(gene)
            # mutation indexes
            # having at least one mutation
            mutations = int(self.mutation_rate*gene_size)
            if mutations == 0:
                mutations = 1
            indexes = self.rng.integers(low=0, high=gene_size, size=mutations)
            # flip the bits at indexes
            # note, ~ is like np.invert()
            gene[indexes] = ~gene[indexes]
            # append the new individual
            new_population.append(gene)
        return new_population


class Removal_operator(Genetic_operator):
    """
    This class represents a gene removal operator
    """

    def __init__(self, num_genes_to_remove, remove_last=True):
        """
        Parameters
        ----------
        num_genes_to_remove: int
            number of genes to remove
        remove_last: bool
            remove the last or first individuals
            since list is sorded by fitness remove_last
            will remove the not fit ones
        """
        self.num_genes_to_remove = num_genes_to_remove
        self.remove_last = remove_last

    def operate(self, old_population, new_population):
        """
        Removes N genes from the actual gene stack
        """
        # remove
        if self.remove_last:
            return new_population[:len(new_population)-self.num_genes_to_remove]
        return new_population[self.num_genes_to_remove:]


class Single_point_crossover_operator(Genetic_operator):
    """
    This class represents a single point (random) crossover operator.
    """

    def __init__(self, num_new_offsprings, rng_seed=9):
        """
        Parameters
        ----------
        num_new_offsprings: int
            number of new individuals made
        """
        self.num_offsprings = num_new_offsprings
        self.rng = np.random.default_rng(rng_seed)

    def operate(self, old_population, new_population):
        """
        Apply crossover operator with genes from the old population
        and append the newly generated genes into the population.
        """
        # parent indexes in the population
        # FIXME:
        # do not always take the first one take with ascending
        # propability
        pop_index = 0
        for _ in range(self.num_offsprings):
            # get parent genes
            parent_0 = old_population[pop_index]
            parent_1 = old_population[pop_index+1]
            pop_index += 2
            # have k point wise selection
            k_point = self.rng.integers(low=0, high=parent_0.size, size=1)[0]
            child = np.concatenate((parent_0[:k_point], parent_1[k_point:]))
            # add gene to population
            new_population.append(child)
        return new_population
