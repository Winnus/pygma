# size
# mpiexec -n 4 python mpi_test.py
import numpy as np
import time
from enum import IntEnum
from mpi4py import MPI
# for pool approach
from mpi4py.futures import MPIPoolExecutor


def main_data_one():
    # here the data is splittet into one genome at a time and transmitted to the workers
    class mpi_tags(IntEnum):
        CONTINUE_PROCESSING = 10
        DATA = 3
        DATA_RETURN = 4
        IDLE = 9
        EXIT = 6

    comm = MPI.COMM_WORLD
    world_size = comm.Get_size()
    rank = comm.Get_rank()

    if rank == 0:
        # controller
        # worker tuples
        # (pop_index, indiv_index, phase_index, gene)
        num_genomes = 3
        genome = np.array([0, 1, 1, 1, 0, 0], dtype=np.bool_)
        worker_tuples = [[i, i, i, genome] for i in range(num_genomes)]

        # while we have data distribute it to free workers
        # collect finished data as well
        # a gene is computed once the result is written back
        genes_to_compute = len(worker_tuples)
        while genes_to_compute > 0:
            # check all workers
            for worker_index in range(1, world_size):
                # check if worker can compute new data (Is idle) and we have data to compute
                if comm.iprobe(source=worker_index, tag=mpi_tags.IDLE) and worker_tuples:
                    # eat up that message to clean the pipeline
                    comm.recv(source=worker_index, tag=mpi_tags.IDLE)
                    # pop the next data to distribute
                    data = worker_tuples.pop()
                    # signalling the worker that it should continue to work
                    comm.send(True, dest=worker_index,
                              tag=mpi_tags.CONTINUE_PROCESSING)
                    # send data to worker
                    comm.send(data, dest=worker_index, tag=mpi_tags.DATA)

                # check if we can retrieve a result from the worker
                if comm.iprobe(source=worker_index, tag=mpi_tags.DATA_RETURN):
                    # retrieve the result
                    w_result = comm.recv(
                        source=worker_index, tag=mpi_tags.DATA_RETURN)
                    # write the fitness result into the appropriate individual
                    # TBA
                    print(f'-------------------root got {w_result} from worker {worker_index}')
                    # note that we have computed a gene
                    genes_to_compute -= 1
                    

    if rank > 0:
        # worker
        # loop until stop is signalled
        while True:
            # print(f'worker {rank} loop')
            # signaling that we are waiting for new work
            comm.isend(None, dest=0, tag=mpi_tags.IDLE)
            # we receive a bool if we shall to continue to process
            # to not eat all CPU ressources we sleep a little if no transmission
            while not comm.iprobe(source=0, tag=mpi_tags.CONTINUE_PROCESSING):
                # sleep
                print(f'worker {rank} is sleeping')
                time.sleep(1)
            continue_processing = comm.recv(
                source=0, tag=mpi_tags.CONTINUE_PROCESSING)
            # if we shall not continue stop the worker
            if not continue_processing:
                break
            # get the working data
            data = comm.recv(source=0, tag=mpi_tags.DATA)
            # calculate the data
            print(f'worker {rank} got {data}')
            # return the result
            comm.send([rank, rank, rank], dest=0, tag=mpi_tags.DATA_RETURN)


def main_data_lists():
    class mpi_tags(IntEnum):
        CONTINUE_PROCESSING = 10
        DATA = 3
        EXIT = 6
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # print(rank)
    # print(size)

    # so the idea is to have a dummy here which can be fusioned into the PyGMA
    # this will work as follows:
    # having a rank 0 which will send an numpy array of the form
    # [i_pop, i_indiv, phase_index, [indiv.genome]]
    # this then will be passed to the worker pool (however big the size is)
    # Workers:
    # will receive that array and compute the experiment (import it here) conduct method on it.
    # will communicate back an array to rank 0 of the form:
    # [pop_index, indiv_index, fitness]
    # the rank 0 will then use this array to write back the results (can be done if all workers are finished)
    # otherwise you have to think about a really parralel population phase operation, like distributed population model, but we are going to do distributed islands if needed and then the islands evolve faster or not.

    # todo:
    # since send() will block, how one can distribute the data to the workers in a non blocking schema?
    # and how to receive the results back if one can not send all data at once.
    # one would need to send to 4 workers, wait until one is done and then receive back and send new data'
    # all that in a non blocking implementation
    # maybe one can use mpi.scatter() and gather()
    # then in rank 0 one gets the world size, splits the array of genes into eqaul halfes, distribute them with scatter() to the workers and then once finished gather() them again.

    # since the number of individuals (more islands, population growth) can change dynamically, if using numpy arrays in mpi
    # they need to be dynamically resized. How can this be achieved?
    # since with scatter you do not know what you gonna get as worker there is the only way that you first send
    # a message to the worker that will adjust the receivbuffer appropiately and then send the real dada.

    # so numpy does not work. Therefore use the normal pickle approach.
    # here there are two options.
    # 1. Split the data you need to work on into equal parts for every worker.
    # Then send the data lists and let the workers work on them, gather the results...
    # If for a reason a worker will work to long on his set becaus complicated, the others have to wait.
    # 2. Send always single genes. This way the workers become faster idle again and will only have to wait for the longest gene
    sendbuf = None
    recvbuff = None
    if rank == 0:
        # we are controller
        # make dummy worker tuples
        # [i_pop, i_indiv, phase_index, [indiv.genome]]
        #         num_genomes = 3
        #         genome = np.array([0, 1, 1, 1, 0, 0], dtype=np.bool_)
        #         worker_tuples = np.array([[i, i, i, genome]
        #                                  for i in range(num_genomes)], dtype=object)

        #         # send them
        #         # return will be [pop_index, indiv_index, fitness]
        #         sendbuf = worker_tuples
        #         recvbuff = np.empty([num_genomes, 3])
        #         # comm.Scatter(sendbuf, recvbuff, root=0)

        #         sendbuf = np.array([1, 2, 3])
        #         recvbuff = np.empty([3])
        for i in range(3):
            # send loop
            for worker in range(1, comm.Get_size()):
                print(f'sending to worker {worker}')
                # tell the worker he sould work
                comm.send(True, dest=worker, tag=mpi_tags.CONTINUE_PROCESSING)
                # send the working data
                comm.send([1, 2, 3], dest=worker, tag=3)

            # receive loop
            results = []
            for worker in range(1, comm.Get_size()):
                r = comm.recv(source=worker, tag=4)
                results.append(r)

            print(results, i)
    if rank > 0:
        # we are worker
        # loop until stop is signalled
        while True:
            print(f'worker {rank} loop')
            # receive a bool if we want to continue to process
            # to not eat all CPU ressources we sleep a little if no transmission
            while not comm.iprobe(source=0, tag=mpi_tags.CONTINUE_PROCESSING):
                # sleep
                print(f'worker {rank} is sleeping')
                time.sleep(1)

            continue_processing = comm.recv(
                source=0, tag=mpi_tags.CONTINUE_PROCESSING)
            # if we shall not continue stop the worker
            if not continue_processing:
                break
            # get the working data
            data = comm.recv(source=0, tag=3)
            # calculate the data
            print(data)
            # return the result
            comm.send([rank, rank, rank], dest=0, tag=4)


def main_MPIPool():
    # here the idea is to use the mpipool executor which is inside the mpi4py lib

    # make dummy worker tuples
    # [i_pop, i_indiv, phase_index, [indiv.genome]]
    genome = np.array([0, 1, 1, 1, 0, 0], dtype=np.bool_)
    worker_tuples = [[i, i, i, genome] for i in range(6)]

    # save worker futures
    futures = []
    # starting a worker for each individual
    with MPIPoolExecutor() as executor:
        for w_tuple in worker_tuples:
            futures.append(
                executor.submit(
                    _instantiate_individual_mpi_pool_process,
                    pop_index=w_tuple[0],
                    indiv_index=w_tuple[1],
                    phase_index=w_tuple[2],
                    gene=w_tuple[3],
                )
            )

    # write back the fitness values into the individuals
    for future in futures:
        # result will be a tuple (pop_index, indiv_index, fitness)
        result = future.result()
        print(result)


def _instantiate_individual_mpi_pool_process(pop_index, indiv_index, phase_index, gene):

    # do something...
    print(f'worker {pop_index}')

    # (pop_index, indiv_index, fitness)
    fitness = pop_index
    return (pop_index, indiv_index, fitness)


if __name__ == '__main__':
    # main_MPIPool()
    # main_data_lists()
    main_data_one()
